package com.technoidentity.firstrestapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FirstRestApiApplication

fun main(args: Array<String>) {
	runApplication<FirstRestApiApplication>(*args)
}
