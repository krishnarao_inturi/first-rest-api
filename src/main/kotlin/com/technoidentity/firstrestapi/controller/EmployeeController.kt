package com.technoidentity.firstrestapi.controller

import com.technoidentity.firstrestapi.entity.Employee
import com.technoidentity.firstrestapi.service.EmployeeService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(value = ["/api/employees"])
class EmployeeController(val employeeService: EmployeeService) {

    @GetMapping()
    fun getAllEmployees(): List<Employee> = employeeService.getAllEmployees()

    @GetMapping("/{id}")
    fun getEmployeesById(@PathVariable("id") employeeId: Long) : ResponseEntity<Employee> {
        var employee: Employee? = employeeService.getEmployeesById(employeeId);
        if (employee != null) {
            return ResponseEntity(employee, HttpStatus.OK)
        } else {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
    }
    @PostMapping
    fun createEmployee(@RequestBody employee: Employee) : ResponseEntity<Employee> {
        return ResponseEntity.ok(employeeService.createEmployee(employee))
    }
    @PutMapping("/{id}")
    fun updateEmployeeById(@PathVariable("id") employeeId: Long, @RequestBody payload: Employee): Employee =
        employeeService.updateEmployeeById(employeeId, payload)

    @DeleteMapping("/{id}")
    fun deleteEmployeesById(@PathVariable("id") employeeId: Long): Unit =
        employeeService.deleteEmployeesById(employeeId)
}