package com.technoidentity.firstrestapi.repository

import com.technoidentity.firstrestapi.entity.Employee
import org.springframework.data.jpa.repository.JpaRepository

import org.springframework.data.jpa.repository.Query

import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository : JpaRepository<Employee, Long> {
}