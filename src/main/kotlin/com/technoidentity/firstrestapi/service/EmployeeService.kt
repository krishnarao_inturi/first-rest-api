package com.technoidentity.firstrestapi.service

import com.technoidentity.firstrestapi.entity.Employee
import com.technoidentity.firstrestapi.exception.EmployeeNotFoundException
import com.technoidentity.firstrestapi.repository.EmployeeRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class EmployeeService(private  val employeeRepository:EmployeeRepository) {

    fun getAllEmployees(): List<Employee> = employeeRepository.findAll()

   fun getEmployeesById(employeeId: Long): Employee? {
        return employeeRepository.findByIdOrNull(employeeId)
    }
    fun createEmployee(employee: Employee): Employee = employeeRepository.save(employee)

    fun updateEmployeeById(employeeId: Long, employee: Employee): Employee {
        return if (employeeRepository.existsById(employeeId)) {
            employeeRepository.save(
                Employee(
                    id = employee.id,
                    name = employee.name,
                    salary = employee.salary,
                )
            )
        } else throw EmployeeNotFoundException(HttpStatus.NOT_FOUND, "No matching employee was found")
    }

    fun deleteEmployeesById(employeeId: Long) {
        return if (employeeRepository.existsById(employeeId)) {
            employeeRepository.deleteById(employeeId)
        } else throw EmployeeNotFoundException(HttpStatus.NOT_FOUND, "No matching employee was found")
    }
}