package com.technoidentity.firstrestapi.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "employee")
data class Employee (
                     @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
                     val id: Long?,
                     @Column(name = "name", unique = true, nullable = false)
                     val name: String,
                     val salary: Double)