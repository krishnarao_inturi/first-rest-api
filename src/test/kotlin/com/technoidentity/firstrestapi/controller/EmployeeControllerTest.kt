package com.technoidentity.firstrestapi.controller

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.technoidentity.firstrestapi.entity.Employee
import com.technoidentity.firstrestapi.service.EmployeeService

@WebMvcTest
class EmployeeControllerTest(@Autowired val mockMvc: MockMvc) {
    @MockkBean
    lateinit var employeeService: EmployeeService
    val mapper = jacksonObjectMapper()

    val employee = Employee(1,"krishna",25000.00);

    @Test
    fun givenExistingName_whenGetRequest_thenReturnsNameJsonWithStatus200() {
        every { employeeService.getEmployeesById(1) } returns employee;

        mockMvc.perform(get("/api/employees/1"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("krishna"));
    }

    @Test
    fun givenNameDoesntExist_whenGetRequest_thenReturnsStatus400() {
        every { employeeService.getEmployeesById(1) } returns null;

        mockMvc.perform(get("/api/employees/1"))
            .andExpect(status().isBadRequest());
    }


    @Test
    fun whenPostRequestWithEmployeeJson_thenReturnsStatus200() {
        every { employeeService.createEmployee(employee)} returns employee;

        mockMvc.perform(post("/api/employees").content(mapper.writeValueAsString(employee)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("krishna"));
    }
}