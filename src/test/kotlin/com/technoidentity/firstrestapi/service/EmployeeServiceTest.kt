package com.technoidentity.firstrestapi.service


import com.technoidentity.firstrestapi.entity.Employee
import com.technoidentity.firstrestapi.repository.EmployeeRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.data.repository.findByIdOrNull

import org.junit.jupiter.api.Assertions.assertEquals

class EmployeeServiceTest {
    val employeeRepository: EmployeeRepository = mockk();
    val employeeService = EmployeeService(employeeRepository);

    val employee = Employee(4,"VENKAT",20000.00);

    @Test
    fun getByEmployeeId() {
        //given
        every { employeeRepository.findByIdOrNull(1) } returns employee;

        //when
        val result = employeeService.getEmployeesById(1);

        //then
        verify(exactly = 1) { employeeRepository.findByIdOrNull(1) };
        assertEquals(employee, result)
    }
}